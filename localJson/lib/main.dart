import 'package:flutter/material.dart';
import 'helper.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:async';
import 'dart:convert';

void main() {
  runApp(MaterialApp(title: 'My App', home: HomePage()));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List data;
  List unfilterData;
  Future<String> loadJsonData() async {
    var jsonText = await rootBundle.loadString("assets/data.json");
    setState(() {
      data = json.decode(jsonText);
    });
    this.unfilterData = data;
    //print(jsonText);
    return 'success';
  }

  @override
  void initState() {
    this.loadJsonData();
    super.initState();
  }

  searchData(str) {
    var strExitst = str.length > 0 ? true : false;
    if (strExitst) {
      var filterData = [];
      for (var i = 0; i < unfilterData.length; i++) {
        String name = unfilterData[i]['name'].toUpperCase();
        if (name.contains(str.toUpperCase())) {
          filterData.add(unfilterData[i]);
        }
      }
      setState(() {
        this.data = filterData;
      });
    } else {
      setState(() {
        this.data = this.unfilterData;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("MyApp")),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextField(
              //obscureText: true,
              decoration: InputDecoration(
                  //border: OutlineInputBorder(),
                  //labelText: 'Password',
                  hintText: "search"),
              onChanged: (String str) {
                this.searchData(str);
              },
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(children: [
                        ListTile(
                            leading: CircleAvatar(
                                child: Text(data[index]['name'][0])),
                            title: Text(data[index]['name']),
                            subtitle: Text(data[index]['email']))
                      ]);
                    }))
          ],
        ));
  }
}
