import 'package:flutter/material.dart';
import 'package:multipage/main.dart';

class PageOne extends StatefulWidget {
  var data;
  PageOne(this.data);
  @override
  _PageOneState createState() => _PageOneState(this.data);
}

class _PageOneState extends State<PageOne> {
  var data;
  _PageOneState(this.data);

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("PageOne"),
        ),
        body:         
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Title: ${data['title']}"),
              Divider(),
              Text("Description: ${data['body']}"),
              Divider(),
              RaisedButton(
                child: Text("Back to Home"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
        )

        );
  }
}
