import 'package:flutter/material.dart';
import 'package:multipage/pages/page1.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() {
  runApp(MaterialApp(home: Dashboard()));
}

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List posts;
  Future<bool> _getPosts() async {
    String sericeUri = "https://jsonplaceholder.typicode.com/posts";
    var response = await http.get(Uri.parse(sericeUri));

    setState(() {
      posts = json.decode(response.body.toString());
      //print(posts);
    });
    return true;
  }

  @override
  void initState() {
    super.initState();
    this._getPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
        ),
        body: new ListView.builder(
          padding: new EdgeInsets.all(8.0),
          itemCount: posts.length == null ? 0 : posts.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              title: Text(posts[index]['title']),
              //subtitle: Text(posts[index]['email']),
              onTap: () {
                print(posts[index]);
                Route route = MaterialPageRoute(
                    builder: (context) => PageOne(posts[index]));
                Navigator.push(context, route);
              },
            );
          },
        ));

    // multipage with data
    // Center(
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: [
    //       Text("Dashboard"),
    //       RaisedButton(
    //         child: Text("Page 1"),
    //         onPressed: () {
    //           var message = "Hello Shahin";
    //           Route route =
    //             MaterialPageRoute(builder: (context) => PageOne(message));
    //             Navigator.push(context, route);
    //         },
    //       )
    //     ],
    //   ),
    // )
  }
}
