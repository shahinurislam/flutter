import 'package:flutter/material.dart';
import 'helper.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(MaterialApp(title: 'My App', home: HomePage()));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String txt = 'Snackbar show';

  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  _mySnakbar() {
    var _mySnackbar = SnackBar(content: Text("Hi I am snackbar"));
    _scaffoldkey.currentState.showSnackBar(_mySnackbar);
  }

  _myTost() {
    Fluttertoast.showToast(
        msg: "This is Center Short Toast",
        //toastLength: Toast.LENGTH_SHORT,
        //gravity: ToastGravity.CENTER,
        ////timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
       // fontSize: 16.0
       // link https://pub.dev/packages/fluttertoast
       );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldkey,
        appBar: AppBar(
          title: Text("MyApp"),
        ),
        drawer: _homeDrawer(), // drawer
        body: Column(children: [
          Container(
              height: 500,
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  Text(txt),
                  Center(
                    child: ElevatedButton(
                      child: Text("Snackbar Button"),
                      onPressed: () {
                        //print("Button Press");
                        setState(() {                         
                          _myTost();
                        });
                      },
                    ),
                  )
                ],
              ))
        ]));
  }
}

Widget _homeDrawer() {
  return Drawer(
    child: ListView(
      children: [
        Stack(
          children: [
            Image.asset("assets/images/shahin.jpg"),
            Positioned(
                left: 30,
                top: 30,
                child: Container(
                  height: 100,
                  width: 100,
                  child: Image.asset("assets/images/shahin2.png"),
                )),
            Positioned(
              left: 30,
              bottom: 20,
              child: Text(
                "Hi, Md Shahinur Islam",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        ListTile(leading: Icon(Icons.mail), title: Text("Mailbox")),
        Divider(),
        ListTile(leading: Icon(Icons.mic), title: Text("Mic")),
        Divider(),
        ListTile(leading: Icon(Icons.phone), title: Text("Phone")),
        Divider(),
        ListTile(leading: Icon(Icons.update), title: Text("Update")),
        Divider(),
        ListTile(leading: Icon(Icons.chat), title: Text("Chat")),
      ],
    ),
  );
}
