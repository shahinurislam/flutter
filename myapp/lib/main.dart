import 'package:flutter/material.dart';
import 'helper.dart';

void main() {
  runApp(MaterialApp(
    title: 'My App',
    home: HomePage(),
  ));
}

class HomePage extends StatelessWidget {
  final _longText =
      "sLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
  List people = [
    {"name": "sMaryam1", "email": "shahin1@gmail.com"},
    {"name": "xMaryam2", "email": "shahin2@gmail.com"},
    {"name": "dMaryam3", "email": "shahin3@gmail.com"},
    {"name": "fMaryam4", "email": "shahin4@gmail.com"},
    {"name": "vMaryam4", "email": "shahin4@gmail.com"},
    {"name": "bMaryam4", "email": "shahin4@gmail.com"},
    {"name": "nMaryam4", "email": "shahin4@gmail.com"},
    {"name": "iMaryam4", "email": "shahin4@gmail.com"},
    {"name": "kMaryam4", "email": "shahin4@gmail.com"},
    {"name": "lMaryam4", "email": "shahin4@gmail.com"},
    {"name": "kMaryam4", "email": "shahin4@gmail.com"},
    {"name": "lMaryam5", "email": "shahin5@gmail.com"},
    {"name": "uMaryam1", "email": "shahin1@gmail.com"},
    {"name": "yMaryam2", "email": "shahin2@gmail.com"},
    {"name": "rMaryam3", "email": "shahin3@gmail.com"},
    {"name": "eMaryam4", "email": "shahin4@gmail.com"},
    {"name": "wMaryam4", "email": "shahin4@gmail.com"},
    {"name": "wMaryam4", "email": "shahin4@gmail.com"},
    {"name": "eMaryam4", "email": "shahin4@gmail.com"},
    {"name": "tMaryam4", "email": "shahin4@gmail.com"},
    {"name": "yMaryam4", "email": "shahin4@gmail.com"},
    {"name": "uMaryam4", "email": "shahin4@gmail.com"},
    {"name": "gMaryam4", "email": "shahin4@gmail.com"},
    {"name": "jMaryam5", "email": "shahin5@gmail.com"}
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Home')),

        drawer: _homeDrawer(),

        body: ListView(
          children: [
            _foodCard(),
            _foodCard(),
            _foodCard(),
            _foodCard()
          ]
            
        )
        
        
        //_scrollingMenu(),     
        

        /*     Column(
          children: [
            Container(
              height: 500,
              padding: EdgeInsets.all(20),
              child: ListView(
                //scrollDirection: Axis.horizontal,
                children: [
                  _cell(),
                  Divider(),
                  _cell(),
                  Divider(),
                  _cell(),
                  Divider(),
                  _cell(),
                  Divider(),                  
                  _cell(),
                  Divider(),                  
                  _cell(),
                  Divider(),
                  _cell(),
                ],
              ),
            )
           
          ],
        )
*/
        /*      Column(
          children: [
            Stack(
              children: [
                Image.asset("assets/images/shahin.jpg"),
                Positioned(
                    bottom: 40,
                    left: 30,
                    child: Text(
                      "Hellow Flautter",
                      style: TextStyle(
                          color: Color(Helper.getHexToInt("#000000"))),
                    ))
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Text("Features"),
            SizedBox(
              height: 30,
            ),
            Row(
              children: [
                _rowCell("#bb2167"),
                _rowCell("#00f9ff"),
                _rowCell("#baa59f"),
                _rowCell("#000000")
                //SizedBox(width: 10),
              ],
            ),
            SizedBox(
              height: 40,
              width: 40,
              child: Container(
                decoration: BoxDecoration(color: Colors.pink),
              ),
            ),
          ],
        )
*/

        /*  ListView(
        children: [
            Text(_longText+_longText+_longText+_longText+_longText+_longText+_longText+_longText+_longText+_longText+_longText+_longText),
        ],
      )  */

        /*     
      Column(
        children: <Widget>[
          Image.asset("assets/images/shahin.jpg"),
          Container(
            child: Text(_longText,
              textAlign: TextAlign.justify,
            ),
            padding: EdgeInsets.all(20),
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 20, top: 10),
                padding: EdgeInsets.all(5),
                height: 40,
                width: 50,
                decoration: BoxDecoration(
                    //color: const Color(0xff8c89b9),
                    color: Color(Helper.getHexToInt("#bf007c")),
                    borderRadius: BorderRadius.circular(8)),
                child: Icon(Icons.linked_camera),
              ),
              Center(
                child: Container(
                  height: 120.0,
                  width: 120.0,
                  color: Colors.blue[50],
                  child: Align(
                    alignment: Alignment(0.2, 0.6),
                    child: FlutterLogo(
                      size: 60,
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
*/

        ); // Scaffold
  }

  Widget _foodCard(){
    return Material(
      elevation: 8,
      child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset("assets/images/flower.jpg"),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Flower Images", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                          SizedBox(height: 10,),
                          Text("Price \$11", style: TextStyle(fontSize: 14),),
                          SizedBox(height: 10,),
                          Text("Sale 55", style: TextStyle(fontSize: 14),)
                        ],
                      ),
                    ),
                    
                  ],
                ),
              ),
    );        
  }

  Widget _homeDrawer() {
   return Drawer(
      child: ListView(
        children: [
          Stack(
            children: [
              Image.asset("assets/images/shahin.jpg"),
              Positioned(
                  left: 30,
                  top: 30,
                  child: Container(
                    height: 100,
                    width: 100,
                    child: Image.asset("assets/images/shahin2.png"),
                  )),
              Positioned(
                left: 30,
                bottom: 20,
                child: Text(
                  "Hi, Md Shahinur Islam",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          ListTile(leading: Icon(Icons.mail), title: Text("Mailbox")),
          Divider(),
          ListTile(leading: Icon(Icons.mic), title: Text("Mic")),
          Divider(),
          ListTile(leading: Icon(Icons.phone), title: Text("Phone")),
          Divider(),
          ListTile(leading: Icon(Icons.update), title: Text("Update")),
          Divider(),
          ListTile(leading: Icon(Icons.chat), title: Text("Chat")),
        ],
      ),
    );
  }

  Widget _scrollingMenu (){
    return ListView.builder(
          itemCount: people.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(
                    child: Text(people[index]["name"][0]),
                  ),
                  title: Text(people[index]["name"]),
                  subtitle: Text(people[index]["email"]),
                )
              ],
            );
          },
        );
  }

  Widget _cell() {
    return Row(
      children: [
        Container(
          height: 100,
          width: 100,
          decoration:
              BoxDecoration(color: Color(Helper.getHexToInt("#000000"))),
          child: Icon(
            Icons.link,
            color: Color(Helper.getHexToInt("#ffffff")),
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  // ignore: unused_element
  /* Widget _rowCell(String clr) {
    return Expanded(
        child: Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(color: Color(Helper.getHexToInt(clr))),
      child: Icon(
        Icons.link,
        color: Color(Helper.getHexToInt("#ffffff")),
      ),
    ));
  } */
}
