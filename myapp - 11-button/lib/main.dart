import 'package:flutter/material.dart';
import 'helper.dart';

void main() {
  runApp(MaterialApp(title: 'My App', home: HomePage()));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String txt = 'Stateless Widget';

  int count = 0;

  //final GlobalKey<ScaffoldState> _scaffoldkey = Globalkey<ScaffoldState>();

  //_showSnackbar() {
  //  var _mySnakbar = SnackBar(content: Text("Hello"));
 // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //key: _scaffoldkey,
        appBar: AppBar(
          title: Text("MyApp"),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.plus_one),
          onPressed: () {
            setState(() {
              count++;
            });
          },
        ),
        body: Column(children: [
          Container(
              height: 500,
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  Text(txt),
                  Text("Count: ${count}"),
                  ElevatedButton(
                    child: Text("Count ++ "),
                    onPressed: () {
                      //print("Button Press");
                      setState(() {
                        txt = 'Stateful widget';
                        count++;
                      });
                     // _showSnackbar();
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  FlatButton(
                    child: Text("Flat Button For Count --"),
                    onPressed: () {
                      setState(() {
                        count--;
                      });
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  IconButton(
                    icon: Icon(Icons.account_balance),
                    onPressed: () {
                      setState(() {
                        count++;
                      });
                    },
                  )
                ],
              ))
        ]));
  }
}

Widget _homeDrawer() {
  return Drawer(
    child: ListView(
      children: [
        Stack(
          children: [
            Image.asset("assets/images/shahin.jpg"),
            Positioned(
                left: 30,
                top: 30,
                child: Container(
                  height: 100,
                  width: 100,
                  child: Image.asset("assets/images/shahin2.png"),
                )),
            Positioned(
              left: 30,
              bottom: 20,
              child: Text(
                "Hi, Md Shahinur Islam",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        ListTile(leading: Icon(Icons.mail), title: Text("Mailbox")),
        Divider(),
        ListTile(leading: Icon(Icons.mic), title: Text("Mic")),
        Divider(),
        ListTile(leading: Icon(Icons.phone), title: Text("Phone")),
        Divider(),
        ListTile(leading: Icon(Icons.update), title: Text("Update")),
        Divider(),
        ListTile(leading: Icon(Icons.chat), title: Text("Chat")),
      ],
    ),
  );
}
